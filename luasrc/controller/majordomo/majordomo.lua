module("luci.controller.majordomo.majordomo", package.seeall);

require("os");
require("uci");

package.path = package.path .. ';/usr/share/lcollect/lua/?.lua';
require("majordomo_lib");

DEFAULT_SORTING = "d_count";

local prefixes = {
	monthly = MONTHLY_PREFIX,
	daily = DAILY_PREFIX,
	hourly = HOURLY_PREFIX
}

local subclasses = {
	monthly = "daily",
	daily = "hourly"
}

local superclasses = {
	hourly = "daily",
	daily = "monthly",
}

function index()
	entry({"admin", "statistics"}, firstchild(), _("Statistics"), 80).dependent=false;
	entry({"admin", "statistics", "majordomo"}, firstchild(), _("Majordomo"), 1).dependent=false;
	entry({"admin", "statistics", "majordomo", "overview"}, call("overview"), _("Overview"), 10).dependent=false;
	entry({"admin", "statistics", "majordomo", "details"}, call("details")).dependent=false;
	entry({"admin", "statistics", "majordomo", "settings"}, cbi("majordomo/settings"), _("Settings"), 50).dependent=false;
end

function parse_query_string()
	local vars = { };
	local qs = luci.http.getenv("QUERY_STRING");
	if not qs then
		return vars;
	end
	for pair in qs:gmatch('([^&]+)') do
		local k, v = pair:match('([^=]+)=(.*)');
		vars[k] = v;
	end

	return vars;
end

function get_requested_page()
	local req = luci.dispatcher.context.request;
	return req[4];
end

function sum_of_items(items)
	local sum = { d_count = 0, d_size = 0, d_data_size = 0, u_count = 0, u_size = 0, u_data_size = 0 };
	for _, value in pairs(items) do
		sum.d_count = sum.d_count + value.d_count;
		sum.d_size = sum.d_size + value.d_size;
		sum.d_data_size = sum.d_data_size + value.d_data_size;
		sum.u_count = sum.u_count + value.u_count;
		sum.u_size = sum.u_size + value.u_size;
		sum.u_data_size = sum.u_data_size + value.u_data_size;
	end

	return sum;
end

function build_url(page, class, id, client, sort_by)
	return string.format("%s?class=%s&amp;id=%s&amp;client=%s&amp;sort=%s", luci.dispatcher.build_url('admin', 'statistics', 'majordomo', page), class or "", id or "", client or "", sort_by or DEFAULT_SORTING);
end

function build_sort_by_url(get, sort_by)
	return build_url(get_requested_page(), get["class"], get["id"], get["client"], sort_by);
end

function id_of_superlevel(id)
	return string.sub(id, 1, -4);
end

function get_subclass_list(path, class, id)
	local ids = { };
	local path_str;
	local parse_str;

	if class == "hourly" then
		return nil;

	elseif class == "daily" then
		path_str = "'" .. path .. "/" .. prefixes["hourly"] .. id .. "-'*";
		parse_str = prefixes["hourly"] .. "([%d+%-]+)$";

	elseif class == "monthly" then
		path_str = "'" .. path .. "/" .. prefixes["daily"] .. id .. "-'*";
		parse_str = prefixes["daily"] .. "([%d%-]+)$";

	end

	local handle = io.popen("/bin/ls " .. path_str, "r");
	for line in handle:lines() do
		local parsed = line:match(parse_str);
		table.insert(ids, parsed);
	end
	handle:close();

	-- Return error (nil) for empty table
	if next(ids) == nil then
		return nil;
	end

	return ids;
end

function get_month_origin(db_path, id)
	local path = db_path .. "/" .. MONTHLY_ORIGIN_PREFIX .. id;
	local handle = io.open(path, "r");
	if not handle then
		return nil;
	end
	local timestamp = handle:read("*number");
	handle:close();

	if not timestamp then
		return nil;
	end

	return timestamp;
end

function pretty_print(class, id)
	if class == "monthly" then
		return id;

	elseif class == "daily" then
		return id;

	elseif class == "hourly" then
		local day_id, hour = id:match("([%d%-]+)-(%d+)$");
		return string.format("%s %s:00", day_id, hour);

	else
		return nil;
	end
end

function overview()
	local get_variables = parse_query_string();

	local stats = { };
	local db_path, make_lookup_mac, _, _ = majordomo_get_configuration();
	local static_names = get_static_names_list();

	-- Prepare caches
	local macdb = get_inst_macdb();
	if make_lookup_mac then
		macdb:deserialize();
	end

	-- Decision about sorting
	local sort_by = get_variables["sort"] or DEFAULT_SORTING;

	local handle = io.popen("/bin/ls '" .. db_path .. "/" .. MONTHLY_PREFIX .. "'* | sort -r", "r");
	local iter = 1;
	for file in handle:lines() do
		local db = { };
		local file_year, file_month = file:match(MONTHLY_PREFIX .. "(%d+)-(%d+)$");
		local month = string.format("%d-%02d", file_year, file_month);
		stats[iter] = { month = month, data = { }, since = get_month_origin(db_path, month) };
		local month_items = { };
		read_file(db, file);
		for client, items in pairs(db) do
			month_items[client] = sum_of_items(items);
		end
		stats[iter].data = get_sorted_items(month_items, sort_by);
		iter = iter + 1;
	end
	handle:close();

	luci.template.render("majordomo/index", {
		-- Functions
		build_url = build_url,
		build_sort_by_url = build_sort_by_url,
		-- Instances
		macdb = macdb,
		-- Variables
		get_variables = get_variables,
		stats = stats,
		make_lookup_mac = make_lookup_mac,
		static_names = static_names
	});

	-- Cache cleanup
	if make_lookup_mac then
		macdb:serialize();
	end
end

function details()
	local get_variables = parse_query_string();

	local stats = { };
	local db_path, make_lookup_mac, _, _ = majordomo_get_configuration();
	local static_names = get_static_names_list();

	-- Prepare caches
	local macdb;
	if make_lookup_mac then
		macdb = get_inst_macdb();
		macdb:deserialize();
	end

	-- Decision about sorting
	local sort_by = get_variables["sort"] or DEFAULT_SORTING;

	-- Load and process data
	local db = { };
	local data_ok = true;
	if not read_file(db, db_path .. "/" .. prefixes[get_variables["class"]] .. get_variables["id"]) then
		data_ok = false;
	end

	if not db[get_variables["client"]] then
		data_ok = false;
	end

	if data_ok then
		stats = get_sorted_items(db[get_variables["client"]], sort_by);
	end

	local subclass_list = get_subclass_list(db_path, get_variables["class"], get_variables["id"]);
	local class = get_variables["class"];

	luci.template.render("majordomo/details", {
		-- Functions
		split_key = split_key,
		build_url = build_url,
		build_sort_by_url = build_sort_by_url,
		pretty_print = pretty_print,
		id_of_superlevel = id_of_superlevel,
		-- Instances
		macdb = macdb,
		-- Variables
		class = class,
		subclasses = subclasses,
		superclasses = superclasses,
		get_variables = get_variables,
		subclass_list = subclass_list,
		data_ok = data_ok,
		stats = stats,
		make_lookup_mac = make_lookup_mac,
		static_names = static_names
	});

	-- Cache cleanup
	if make_lookup_mac then
		macdb:serialize();
	end
end
