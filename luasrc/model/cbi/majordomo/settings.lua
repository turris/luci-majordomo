package.path = package.path .. ';/usr/share/lcollect/lua/?.lua';
require("majordomo_lib");

local m, s, o;
m = Map("majordomo", translate("Majordomo - settings"));

-- Define callback function
-- Nothing for now
--[[
function m.on_after_commit(self)
	luci.sys.call("/etc/init.d/rainbow restart");
end
]]

-- Start to build form and page
s = m:section(TypedSection, "db", translate("Majordomo DB"));
	s.addremove = false;
	s.anonymous = true;

o = s:option(Value, "path", translate("Path"), translate("Path to majordomo DB. Set it to permanent storage like USB disk to preserve data between reboots."));
o = s:option(Value, "max_items_per_client", translate("Max items per client"), translate("Maximum number of items that are stored for every client."));
o = s:option(Value, "store_hourly_files", translate("Number of stored hourly files"));
o = s:option(Value, "store_daily_files", translate("Number of stored daily files"));
o = s:option(Value, "store_monthly_files", translate("Number of stored monthly files"));

s = m:section(TypedSection, "lookup", translate("Lookup"), translate("Enables data lookup at network. Web GUI provides more information if enabled but page loading is slower."));
	s.addremove = false;
	s.anonymous = true;

--[[
	This could be checkbox, but it is broken in luci
]]
o = s:option(ListValue, "make_lookup_dns", translate("Make lookup of reverse DNS records"), translate("Translates IP addresses to domain names. Majordomo will use local DNS resolver."));
	o:value(1, translate('Enable'));
	o:value(0, translate('Disable'));

--[[
	Prepare MAC cache
]]
local macdb = get_inst_macdb();
macdb:deserialize();

s = m:section(TypedSection, "static_name", translate("Custom device names"), translate("Here you can add your own names for the devices displayed in the list."));
	s.addremove = true;
	s.anonymous = true;
o = s:option(Value, "mac", translate("MAC address"));
	for k, v in pairs(macdb:items()) do
		local desc = k .. " (" .. v .. ")";
		o:value(k, desc);
	end
o = s:option(Value, "name", translate("Device name"));

return m;
